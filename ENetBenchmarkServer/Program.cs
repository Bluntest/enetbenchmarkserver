﻿using DisruptorUnity3d;
using ENet;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace ENetBenchmarkServer
{
    class Program
    {

		static List<Peer> peers = new List<Peer>();
										
		static int PacketQueueCapacity = 100000;
		static RingBuffer<Packet> packetQueue = new RingBuffer<Packet>(PacketQueueCapacity);

		static int PacketTestCount = 1000;
		static int TICK_RATE = 5;

		static void QueueMessageThread()
        {
			Stopwatch tick = new Stopwatch();
			tick.Start();

			while(true)
            {

				if(tick.ElapsedMilliseconds >= 1000 / TICK_RATE)
                {
					Packet packet = default(Packet);
					packet.Create(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 }, PacketFlags.None);
					for(int i = 0; i < peers.Count; i++)
                    {
						for(int n = 0; n < PacketTestCount; n++)
						{
							packetQueue.Enqueue(packet);
						}
                    }

					tick.Restart();

                }
				Thread.Sleep(1);
            }
        }

		static void DequeueThread()
        {
			while (true)
			{
				Packet p = packetQueue.Dequeue();
			}
		}

		static void NetworkThread()
        {
			Stopwatch sw = new Stopwatch();

			uint lastPacketsSent = 0;
			using (Host server = new Host())
			{
				Address address = new Address();

				address.Port = 10001;
				server.Create(address, 2000);

				Event netEvent;
				
				while (!Console.KeyAvailable)
				{

					bool polled = false;

					while (!polled)
					{
						if (server.CheckEvents(out netEvent) <= 0)
						{
							if (server.Service(15, out netEvent) <= 0)
								break;

							polled = true;
						}

						switch (netEvent.Type)
						{
							case EventType.None:
								break;

							case EventType.Connect:
								Console.WriteLine("Client connected - ID: " + netEvent.Peer.ID + ", IP: " + netEvent.Peer.IP);
								peers.Add(netEvent.Peer);
								break;

							case EventType.Disconnect:
								Console.WriteLine("Client disconnected - ID: " + netEvent.Peer.ID + ", IP: " + netEvent.Peer.IP);
								break;

							case EventType.Timeout:
								Console.WriteLine("Client timeout - ID: " + netEvent.Peer.ID + ", IP: " + netEvent.Peer.IP);
								break;

							case EventType.Receive:
								//Console.WriteLine("Packet received from - ID: " + netEvent.Peer.ID + ", IP: " + netEvent.Peer.IP + ", Channel ID: " + netEvent.ChannelID + ", Data length: " + netEvent.Packet.Length);
								netEvent.Packet.Dispose();
								break;
						}




					}

					/*foreach (Peer _peer in peers)
					{

						while (packetQueue.Count > 0)
						{
							Packet p = packetQueue.Dequeue();
							_peer.Send(0, ref p);
						}
					}*/


					sw.Start();
					foreach (Peer _peer in peers.ToArray())
                    {
                        Packet packet = default(Packet);
                        packet.Create(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 }, 12, PacketFlags.None);
                        for (int i = 0; i < 1000; i++)
                        {
                            _peer.Send(0, ref packet);
                        }
						server.Flush();
					}
                    


                    server.Flush();

					
					Console.WriteLine("Time to send: {0}ms | Sent: {1} | Messages: {2} | Peers: {3}", sw.ElapsedMilliseconds,server.PacketsSent - lastPacketsSent,1000,peers.Count);
					lastPacketsSent = server.PacketsSent;
					sw.Reset();
				}
			}
		}
	

		static void Main(string[] args)
		{
			ENet.Library.Initialize();


			Thread networkThread = new Thread(NetworkThread);
			networkThread.Start();

			Thread queueMessageThread = new Thread(QueueMessageThread);
			queueMessageThread.Start();


			Stopwatch outputTick = new Stopwatch();
			outputTick.Start();
			while (true)
			{
				if (outputTick.ElapsedMilliseconds >= 1000)
				{
					Console.WriteLine("[PacketMessageQueue] Count: {0}/{1}", packetQueue.Count, packetQueue.Capacity);
					outputTick.Restart();

				}
				Thread.Sleep(1);
			}


			Stopwatch sw = new Stopwatch();

		}
    }
}
